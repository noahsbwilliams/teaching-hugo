# Setting up the development environment



## Apps and technologies used

- Windows 10 OS
- Chocolatey package manager
- Atom text editor
- Git version control
- Hugo static site generator

## Steps

### 1. Install a package manager

Most servers run Linux, a flexible and popular open-source operating system. We'll get into the importance of this later, but for now, let's get ourselves setup.

 On Linux systems, apps are called **packages**. They are managed by an app called a **package manager**. Think of this like an App Store. This app handles downloading, installing, and updating of apps. 

Windows doesn't come with a package manager, so we'll install one of our own, called **Chocolatey**. 

Head over to Chocolatey's [website](chocolatey.org/install),  and copy the script below "Install with cmd.exe". 

Now, open a command prompt as administrator. Type ```cmd``` into the start menu, right-click the Command Prompt and choose `Run as Adminstrator`. 

Optional:

Want an app-store like interface for Chocolatey? Run the command ```choco install chocolateygui``` to install the gui, or graphical user interface, for Chocolatey. A new app will appear in the Start menu 

### 2. Install a **text editor**

For this tutorial, we'll use **Atom**, a free and open source text editor developed by GitHub.

We can use **Chocolatey** to install Atom. To do this, open Powershell as administrator and type ```choco install atom```. When prompted, type ```y``` to accept the license and run the installation script. A few seconds later, you'll notice a new icon for Atom in the start menu.

### 3. Install Git

**Git** is a source code management app, and is a *dependancy*, or part of, Hugo. We'll get to its importance later.

For now, let's install it using the same process we did for Atom. Type ```choco install git``` into your Powershell prompt.



### 4. Install **Hugo**

**Hugo** is a static site generator - aka, an app that transforms our plain ```markdown``` files into beautiful static webpages.

You get the idea - in the Powershell command line, type  ```choco install hugo```. Type ```y``` to run the install script. Boom!



Note: Hugo is a *headless* (no gui) app, so no new icons will appear in the start menu.



### Congratulations! We're ready to start learning Hugo!
